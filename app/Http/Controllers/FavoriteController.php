<?php

namespace App\Http\Controllers;
use App\Question;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    public function __cunstroct()
    {
        # code...
        $this->middleware('auth');
    }
    public function store(Question $question)
    {
        # code...
        $question->favorites()->attach(auth()->id());
        return back();

    }
    
    public function destroy(Question $question)
    {
        # code...
        $question->favorites()->detach(auth()->id());
        return back();

    }
}
