<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable =['body','user_id'];
    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getBodyHtmlAttribute()
    {
        return \Parsedown::instance()->text($this->body);
    }

    public static function boot()
    {
        parent::boot();
        static::created(function($answer){
            $answer->question->increment('answers_count');
            
        });

        static::deleted(function($answer){
            /*$question = $answer->question;
            $question->decrement('answers_count');
            if ($question->best_answer_id === $answer->id) {
                $question->best_asnwer_id = NULL;
                $question->save();
            }*/

            /**
             * lines kode di atas tidak diperlukan lagi karena di file migrasi add foreign
             * best answer sudah di setting dengan onDelete Set null. 
             */

            $answer->question->decrement('answers_count');

        });
        
    }

    public function getCreatedDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    
    public function getStatusAttribute()
    {
        # code...
        //return $this->id === $this->question->best_answer_id ? 'vote-accepted' :'';
        return $this->isbest() ? 'vote-accepted' :'';
    }
    
    public function getIsBestAttribute()
    {
        return $this->isBest();
    }

    public function isBest()
    {
        # code...
        return $this->id === $this->question->best_answer_id;
    }
}
